﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;

    public GameObject arCamera;
        
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {



        
    }

    public void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {

            if (hit.transform.name == "Capsule")
            {

                Destroy(hit.transform.gameObject);

            }

            if (hit.transform.name == "Capsule02")
            {

                Destroy(hit.transform.gameObject);

            }

            if (hit.transform.name == "Capsule03")
            {

                Destroy(hit.transform.gameObject);

            }


        }
        if (hit.transform.name == "Capsule")
        {

            enemy2.SetActive(true);

        }

        if (hit.transform.name == "Capsule02")
        {

            enemy3.SetActive(true); 

        }

    }
}
